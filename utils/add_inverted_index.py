#!/usr/bin/env python3
from numpy.random import randint
import random
import numpy as np
import time
from tqdm import tqdm
import boto3
from boto3.dynamodb.conditions import Key

def main():
    dynamodb = boto3.client('dynamodb', endpoint_url="http://localhost:8000")
    try:
        dynamodb.update_table(
            TableName='vm-tag-item',
            AttributeDefinitions=[
                {
                    "AttributeName": "pk",
                    "AttributeType": "N"
                },
                {
                    "AttributeName": "pk",
                    "AttributeType": "N"
                }
            ],
            GlobalSecondaryIndexUpdates=[
                {
                    "Create": {
                        "IndexName": "InvertedIndex",
                        "KeySchema": [
                            {
                                "AttributeName": "sk",
                                "KeyType": "HASH"
                            },
                            {
                                "AttributeName": "pk",
                                "KeyType": "RANGE"
                            }
                        ],
                        "Projection": {
                            "ProjectionType": "ALL"
                        },
                        "ProvisionedThroughput": {
                            "ReadCapacityUnits": 1,
                            "WriteCapacityUnits": 1
                        }
                    }
                }
            ],
        )
        print("Table updated successfully.")
    
    except Exception as e:
        print("Could not update table. Error:")
        print(e)


if __name__ == "__main__":
    main()
