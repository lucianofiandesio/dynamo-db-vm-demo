aws dynamodb create-table \
      --table-name vm-tag-item \
      --attribute-definitions AttributeName=pk,AttributeType=N AttributeName=sk,AttributeType=N \
      --key-schema AttributeName=pk,KeyType=HASH AttributeName=sk,KeyType=RANGE \
      --billing-mode PAY_PER_REQUEST \
      --endpoint-url http://localhost:8000
