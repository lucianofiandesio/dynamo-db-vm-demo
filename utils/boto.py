#!/usr/bin/env python3
from numpy.random import randint
import random
import numpy as np
import time
from tqdm import tqdm
import boto3

def main():
    dynamodb = boto3.resource('dynamodb', endpoint_url="http://localhost:8000")
    table = dynamodb.Table('vm-tag-item')
    response = table.put_item(
       Item={
            'tag-id': 1000,
            'item-id': 200
            }
    )
    print(response)
    #ddb = boto3.client('dynamodb', endpoint_url='http://localhost:8000')
    #response = ddb.list_tables()
    #print(response)

if __name__ == "__main__":
    main()
