#!/usr/bin/env python3
from numpy.random import randint
from numerize import numerize
import random
import numpy as np
import time
from tqdm import tqdm
import boto3

TABLE_NAME="vm-tag-item"
BILLION = 1000000000
ITEM_SIZE = 1000000#BILLION
MIN_TAG_PER_ITEM=4
MAX_TAG_PER_ITEM=16
BATCH_SIZE=100

def main():
    t0 = time.time() 
    item_ids = randint(1, ITEM_SIZE*2, ITEM_SIZE) 
    d = time.time() - t0
    print ("generated " + numerize.numerize(ITEM_SIZE) + " item ids: %.2f s." % d)

    # generated 25K random tag id
    tag_ids = randint(100, 50000, 25000)
    
    # dynamodb - open connection
    dynamodb = boto3.resource('dynamodb', endpoint_url="http://localhost:8000")
    table = dynamodb.Table(TABLE_NAME)
    
    buffer = []
    for item_id in tqdm(item_ids):
        #print(len(buffer))
        if len(buffer) >= BATCH_SIZE:
            try:
                batch_write(table, buffer)
            except Exception as e:
                pass # do nothing
            finally:
                buffer.clear()
                
        # gen random number of tags for item
        number_of_tags = random.randint(MIN_TAG_PER_ITEM, MAX_TAG_PER_ITEM)
    
        # pick random tag ids from array 
        selected_tags = random_sample(tag_ids, number_of_tags)

        for tag_id in selected_tags:
            buffer.append((item_id, tag_id))

    print("done in %.2f s." % (time.time() -t0))

def batch_write(table, items):
    with table.batch_writer() as batch:
        for tuple in items:
            batch.put_item(
                Item={
                    'pk': tuple[0].item(),
                    'sk': tuple[1].item()
                }
            )




def random_sample(arr: np.array, size: int = 1) -> np.array:
    return arr[np.random.choice(len(arr), size=size, replace=False)]

if __name__ == "__main__":
    main()
